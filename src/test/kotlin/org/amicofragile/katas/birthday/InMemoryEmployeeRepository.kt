package org.amicofragile.katas.birthday

import java.time.LocalDate

class InMemoryEmployeeRepository(private val employees: List<Employee>) : EmployeeRepository {
    override fun loadEmployeesByBirthday(birthday: LocalDate): Collection<Employee> {
        return employees.filter { e -> e.birthDay == birthday }
    }
}