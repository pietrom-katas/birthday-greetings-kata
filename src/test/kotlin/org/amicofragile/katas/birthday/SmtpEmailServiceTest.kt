package org.amicofragile.katas.birthday

import org.junit.Test
import com.icegreen.greenmail.util.GreenMail
import com.icegreen.greenmail.util.ServerSetup
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.AfterClass
import org.junit.BeforeClass
import java.net.ServerSocket
import java.io.IOException


class SmtpEmailServiceTest {
    companion object {
        @Throws(IOException::class)
        private fun findRandomOpenPortOnAllLocalInterfaces(): Int {
            ServerSocket(0).use { socket ->
                return socket.localPort
            }
        }
        private const val SmtpHost = "localhost"
        private const val SmtpUsername = "mailmaster"
        private const val SmtpPassword = "mailpass"
        private val Port = findRandomOpenPortOnAllLocalInterfaces()
        var greenMail: GreenMail? = null

        @BeforeClass
        @JvmStatic
        fun initSmtpServer() {
            greenMail = GreenMail(ServerSetup(Port, SmtpHost, ServerSetup.PROTOCOL_SMTP))
            greenMail!!.setUser(SmtpUsername, SmtpPassword)
            greenMail!!.start()
        }

        @AfterClass
        @JvmStatic
        fun stopSmtpServer() {
            greenMail?.stop()
        }
    }

    private val sender:EmailService = SmtpEmailService(SmtpHost, Port, SmtpUsername, SmtpPassword)

    @Test
    fun shouldSendNothing() {
        assertThat(receivedMessagesCount, `is`(equalTo(0)))
    }

    @Test
    fun shouldSendEmailMessage() {
        sender.sendMessage(Email("Pietro", "pietro@foobar.com", "Some text for the body"))
        assertThat(receivedMessagesCount, `is`(equalTo(1)))
    }

    private val receivedMessagesCount : Int
        get() = greenMail!!.receivedMessages.count()
}