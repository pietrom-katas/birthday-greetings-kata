package org.amicofragile.katas.birthday

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class EmailComposerTest {
    private val MailTextForJohn: String = """
        Subject: Happy birthday!

        Happy birthday, dear John!
    """.trimIndent()

    @Test
    fun mailTextIsCorrect() {
        val composer = EmailComposer()
        val text = composer.composeGreetingsEmailFor("John")
        assertThat(text, `is`(equalTo(MailTextForJohn)))
    }
}