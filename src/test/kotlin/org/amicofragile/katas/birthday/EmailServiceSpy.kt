package org.amicofragile.katas.birthday

class EmailServiceSpy : EmailService {
    override fun sendMessage(msg: Email) {
        sentEmails.add(msg)
    }

    val sentEmails = ArrayList<Email>()
    val sentEmailsCount
        get() = sentEmails.size


}