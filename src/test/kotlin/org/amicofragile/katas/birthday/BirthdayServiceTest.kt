package org.amicofragile.katas.birthday

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import java.time.LocalDate
import java.util.Arrays.asList

class BirthdayServiceTest {
    @Test
    fun shouldSendEmailToNobody() {
        val repo : EmployeeRepository = InMemoryEmployeeRepository(asList(
                Employee("John", "Doe", "a@foo.com", LocalDate.of(1978, 1, 1)),
                Employee("John", "Doe", "a@foo.com", LocalDate.of(1980, 1, 1))
        ))

        val emailService = EmailServiceSpy()
        val sut = BirthdayService(repo, emailService)
        sut.sendGreetings(LocalDate.of(1978, 3, 19))
        assertThat(emailService.sentEmailsCount, `is`(equalTo(0)))
    }

    @Test
    fun shouldSendEmailToEmployeesWithMatchingBirtdDate() {
        val repo : EmployeeRepository = InMemoryEmployeeRepository(asList(
                Employee("John", "Doe", "a@foo.com", LocalDate.of(1978, 3, 19)),
                Employee("Jim", "Doe", "b@foo.com", LocalDate.of(1980, 1, 1)),
                Employee("Jules", "Doe", "c@foo.com", LocalDate.of(1980, 1, 1)),
                Employee("Jack", "Doe", "d@foo.com", LocalDate.of(1978, 3, 19))
        ))

        val emailService = EmailServiceSpy()
        val sut = BirthdayService(repo, emailService)
        sut.sendGreetings(LocalDate.of(1978, 3, 19))
        assertThat(emailService.sentEmailsCount, `is`(equalTo(2)))
        assertThat(emailService.sentEmails.map { msg -> msg.to }, `is`(equalTo(listOf("John", "Jack"))))
    }
}