package org.amicofragile.katas.birthday

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.emptyCollectionOf
import org.junit.Test
import java.io.FileInputStream
import java.nio.file.Files
import java.nio.file.Path
import java.time.LocalDate

class FileBasedEmployeeRepositoryTest {
    val FileContent: String = """
        last_name, first_name, date_of_birth, email
        Doe, John, 1982/10/08, john.doe@foobar.com
        Ann, Mary, 1975/09/11, mary.ann@foobar.com
        Gwyn, Mr, 1978/03/19, mrgwyn@foobar.com
        Deveria, Ann, 1980/03/19, ann.deveria@foobar.com
    """.trimIndent()

    val FileContentWithCr: String = "last_name, first_name, date_of_birth, email\rDoe, John, 1982/10/08, john.doe@foobar.com\rGwyn, Mr, 1978/03/19, mrgwyn@foobar.com"

    val FileContentWithCrLf: String = "last_name, first_name, date_of_birth, email\r\nDoe, John, 1982/10/08, john.doe@foobar.com\r\nGwyn, Mr, 1978/03/19, mrgwyn@foobar.com"

    var fileName: Path = createTempFileName()

    private fun createTempFileName(): Path {
        val tempDirWithPrefix = Files.createTempDirectory("test")
        return Files.createTempFile(tempDirWithPrefix, "input", ".data")
    }

    @Test
    fun loadEmployeesFromEmptyFile() {
        writeInputData("")
        val repo = FileBasedEmployeeRepository(FileInputStream(fileName.toFile()))
        val employees = repo.loadEmployeesByBirthday(LocalDate.of(2019, 3, 19))
        assertThat(employees, `is`(emptyCollectionOf(Employee::class.java)))
    }

    @Test
    fun loadEmployeesFromFileFilteringByBirthday() {
        writeInputData(FileContent)
        val repo = FileBasedEmployeeRepository(FileInputStream(fileName.toFile()))
        val employees = repo.loadEmployeesByBirthday(LocalDate.of(2019, 3, 19))
        assertThat(employees.size, `is`(equalTo(2)))
        assertThat(employees.map { e -> e.email }, `is`(listOf("mrgwyn@foobar.com", "ann.deveria@foobar.com")))
    }

    @Test
    fun loadEmployeesFromFileUsingCrAsLineSeparator() {
        writeInputData(FileContentWithCr)
        val repo = FileBasedEmployeeRepository(FileInputStream(fileName.toFile()))
        val employees = repo.loadEmployeesByBirthday(LocalDate.of(2019, 3, 19))
        assertThat(employees.size, `is`(equalTo(1)))
    }

    @Test
    fun loadEmployeesFromFileUsingCrLfAsLineSeparator() {
        writeInputData(FileContentWithCrLf)
        val repo = FileBasedEmployeeRepository(FileInputStream(fileName.toFile()))
        val employees = repo.loadEmployeesByBirthday(LocalDate.of(2019, 3, 19))
        assertThat(employees.size, `is`(equalTo(1)))
    }

    private fun writeInputData(content: String) {
        fileName.toFile().writeText(content)
    }
}