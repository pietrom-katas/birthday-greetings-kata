package org.amicofragile.katas.birthday

import java.util.*
import javax.mail.*
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart

class SmtpEmailService(private val host: String, private val port: Int, private val username: String, private val password: String) : EmailService {
    override fun sendMessage(msg: Email) {
        val prop = Properties()
        prop.put("mail.smtp.auth", true)
        //prop.put("mail.smtp.starttls.enable", "true")
        prop.put("mail.smtp.host", host)
        prop.put("mail.smtp.port", port)
        //prop.put("mail.smtp.ssl.trust", "smtp.mailtrap.io")

        val session = Session.getInstance(prop, object : Authenticator() {
            override fun getPasswordAuthentication(): PasswordAuthentication {
                return PasswordAuthentication(username, password)
            }
        })

        val message = MimeMessage(session)
        message.setFrom("me@foobar.com")
        message.setRecipients(Message.RecipientType.TO, msg.address)
        message.subject = "Greetings"
        val mimeBodyPart = MimeBodyPart()
        mimeBodyPart.setContent(msg.body, "text/html")

        val multipart = MimeMultipart()
        multipart.addBodyPart(mimeBodyPart)

        message.setContent(multipart)

        Transport.send(message)
    }

}
