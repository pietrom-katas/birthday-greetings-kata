package org.amicofragile.katas.birthday

class EmailComposer {
    fun composeGreetingsEmailFor(target: String): String {
        return """
            Subject: Happy birthday!

            Happy birthday, dear $target!
        """.trimIndent()
    }
}