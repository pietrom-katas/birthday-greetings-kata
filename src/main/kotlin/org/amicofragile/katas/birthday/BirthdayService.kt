package org.amicofragile.katas.birthday

import java.time.LocalDate

class BirthdayService(private val repo: EmployeeRepository, private val emailService: EmailService) {
    fun sendGreetings(day: LocalDate) {
        for (employee: Employee in repo.loadEmployeesByBirthday(day)) {
            emailService.sendMessage(Email(employee.firstName, employee.email, ""))
        }
    }
}