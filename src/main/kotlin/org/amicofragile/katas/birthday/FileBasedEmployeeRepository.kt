package org.amicofragile.katas.birthday

import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.time.LocalDate

class FileBasedEmployeeRepository(private val input: InputStream) : EmployeeRepository {
    companion object {
        private val NewLineRegex = Regex("[\\n\\r]+")
    }

    override fun loadEmployeesByBirthday(birthday: LocalDate): Collection<Employee> {
        val text = BufferedReader(InputStreamReader(input)).readText()
        val rows = text.split(NewLineRegex).drop(1)
        return rows.map {r -> rowToEmployee(r)}.filter { e -> birtdayMatches(e, birthday) }
    }

    private fun birtdayMatches(e: Employee, birthday: LocalDate): Boolean {
        return e.birthDay.month == birthday.month && e.birthDay.dayOfMonth == birthday.dayOfMonth
    }

    private fun rowToEmployee(row: String): Employee {
        val fields = row.split(',')
        return Employee(fields[1].trim(), fields[0].trim(), fields[3].trim(), birthdayFromString(fields[2].trim()))
    }

    private fun birthdayFromString(s: String): LocalDate {
        val fields = s.split('/')
        val year = Integer.valueOf(fields[0])
        val month = Integer.valueOf(fields[1])
        val day = Integer.valueOf(fields[2])
        return LocalDate.of(year, month, day)
    }
}