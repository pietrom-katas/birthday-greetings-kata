package org.amicofragile.katas.birthday

import java.time.LocalDate

class Employee(val firstName: String, val lastName: String, val email: String, val birthDay: LocalDate)