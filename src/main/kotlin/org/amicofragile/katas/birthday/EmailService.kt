package org.amicofragile.katas.birthday

interface EmailService {
    fun sendMessage(msg: Email)
}