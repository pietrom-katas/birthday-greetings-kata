package org.amicofragile.katas.birthday

import java.io.FileInputStream
import java.time.LocalDate

fun main(args: Array<String>) {
    val fileName = ""
    val employeeRepository = FileBasedEmployeeRepository(FileInputStream(fileName))
    val emailService = WriterBasedEmailService(System.out)
    val birthdayService = BirthdayService(employeeRepository, emailService)
    birthdayService.sendGreetings(today())
}

fun today() = LocalDate.now()