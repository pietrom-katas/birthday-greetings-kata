package org.amicofragile.katas.birthday

import java.time.LocalDate

interface EmployeeRepository {
    fun loadEmployeesByBirthday(birthday: LocalDate) : Collection<Employee>
}